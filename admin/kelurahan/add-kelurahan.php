<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-lg-6">
        <h3 class="panel-title">Tambah Data Kelurahan</h3>
      </div>
      <div class="col-lg-6">
        <a href="?admin=kelurahan">
        <button type="button" class="btn btn-info pull-right">Kembali</button>
        </a>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <form action="kelurahan/act_add_kelurahan.php" method="post">
      <div class="row">
        <div class="col-lg-3">
          <h5>Nama Kelurahan</h5>
        </div>
        <div class="col-lg-9">
          <input name="nama_kelurahan" id="form" type="text" required/>
        </div>
      </div>
	   <div class="row">
        <div class="col-lg-3">
          <h5>Jumlah Laki-laki</h5>
        </div>
        <div class="col-lg-9">
          <input name="laki" id="form" type="text" required/>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <h5>Jumlah Perempuan</h5>
        </div>
        <div class="col-lg-9">
          <input name="perempuan" id="form" type="text" required/>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <h5>Luas Wilayah</h5>
        </div>
        <div class="col-lg-9">
          <input name="luas" id="form" type="text" required/>
        </div>
      </div>
     

      <input name="save" type="submit" class="btn btn-primary" value="Tambah data" />
      <input name="save" type="reset" class="btn btn-success" value="Reset" />
    </form>
  </div>
  <div class="panel-footer"></div>
</div>
