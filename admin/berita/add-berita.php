<script src="berita/ckeditor.js"></script>
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-lg-6">
        <h3 class="panel-title">Tambah Berita</h3>
      </div>
      <div class="col-lg-6">
        <a href="?admin=berita">
        <button type="button" class="btn btn-info pull-right">Kembali</button>
        </a>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <form action="berita/act_add_berita.php" method="post" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-3">
          <h5>Judul</h5>
        </div>
        <div class="col-lg-6">
          <input name="judul" id="form" type="text" class ="form-control" required/>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <h5>Tanggal</h5>
        </div>
        <div class="col-lg-6">
          <input name="tgl" value="<?PHP echo date("Y-m-d"); ?>" readonly="readonly" id="form" type="text" class ="form-control" />
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <h5>Gambar</h5>
        </div>
        <div class="col-lg-6">
          <input name="image" id="brows" type="file"  enctype="multipart/form-data" required/>
          <span class="label label-danger">ukuran gambar max 1MB</span>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <h5>Isi Berita</h5>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          
            <textarea id="editor1" name="isi_berita"></textarea>
        <script>
            CKEDITOR.replace( 'editor1' );
        </script>
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <h5>Penulis</h5>
        </div>
        <div class="col-lg-6">
          <input name="penulis" value="<?PHP echo $_SESSION['username']; ?>" id="form" class ="form-control" type="text" />
        </div>
      </div>

      <input name="save" type="submit" class="btn btn-primary" value="Tambah Berita" />
      <input  type="reset" class="btn btn-success" value="Reset" />
    </form>
  </div>
  <div class="panel-footer"></div>
</div>
