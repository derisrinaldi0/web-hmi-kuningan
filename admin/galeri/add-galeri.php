<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-lg-6">
        <h3 class="panel-title">Tambah Gambar</h3>
      </div>
      <div class="col-lg-6">
        <a href="?admin=user">
        <button type="button" class="btn btn-info pull-right">Kembali</button>
        </a>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <form action="galeri/act_add_galeri.php" method="post" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-3">
          <h5>Pilih Gambar</h5>
        </div>
        <div class="col-lg-9">
          <input name="nama_gambar" id="form" type="file" required/>
        </div>
		
      </div>
	   <div class="row">
        <div class="col-lg-3">
          <h5>Sumber</h5>
        </div>
        <div class="col-lg-9">
          <input name="sumber" id="form" type="text" required/>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <h5>Keterangan</h5>
        </div>
        <div class="col-lg-9">
          <input name="keterangan" id="form" type="text" required/>
        </div>
      </div>
     

      <input name="save" type="submit" class="btn btn-primary" value="Tambah Gambar" />
      <input  type="reset" class="btn btn-success" value="Reset" />
    </form>
  </div>
  <div class="panel-footer"></div>
</div>
