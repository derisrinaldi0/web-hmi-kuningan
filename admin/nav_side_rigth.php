<nav class="navbar-default navbar-static-side" role="navigation">
            <!-- sidebar-collapse -->
            <div class="sidebar-collapse">
                <!-- side-menu -->
                <ul class="nav" id="side-menu">
                    <li>
                        <!-- user image section-->
                        <div class="user-section">
                            <div class="user-section-inner">
                                <img src="assets/img/user.jpg" alt="">
                            </div>
                            <div class="user-info">
                                <div><strong><?php echo $_SESSION['username'];?></strong></div>
                                <div class="user-text-online">
                                    <span class="user-circle-online btn btn-success btn-circle "></span>&nbsp;Online
                                </div>
                            </div>
                        </div>
                        <!--end user image section-->
                    </li>
					<?php
					if($_SESSION['level']=="admin"){
						?>
                    <li class="">
                        <a href="?admin=user"><i class="fa fa-wrench fa-fw"></i>Kelola User</a>
                    </li>
					<?php }?>
					<li class="">
                        <a href="?admin=berita"><i class="fa fa-files-o fa-fw"></i>Kelola Berita</a>
                    </li>
					<li class="">
                        <a href="blank.php"><i class="fa fa-files-o fa-fw"></i>Kelola Profil<span class="fa arrow"></a>
						<ul class="nav nav-second-level">
                            <li>
                                <a href="?admin=visimisi">Visi Misi</a>
                            </li>
                            <li>
                                <a href="?admin=tugas">Tugas Pokok dan Fungsi</a>
                            </li>
                            <li>
                                <a href="?admin=struktur">Struktur Organisasi</a>
                            </li>
                            <li>
                                <a href="?admin=anggota">Anggota</a>
                            </li>
                            <li>
                                <a href="?admin=poker">Program kerja</a>
                            </li>
							
                        </ul>
                        <!-- second-level-items -->
                    </li>
					
					<li class="">
                        <a href="?admin=galeri"><i class="fa fa-files-o fa-fw"></i>Kelola Galeri</a>
                    </li>
					
					<!--<li class="">
                        <a href="blank.php"><i class="fa fa-files-o fa-fw"></i>Kelola Data kelurahan</a>
                    </li>-->
					
                    
                </ul>
                <!-- end side-menu -->
            </div>
            <!-- end sidebar-collapse -->
        </nav>
        <!-- end navbar side -->