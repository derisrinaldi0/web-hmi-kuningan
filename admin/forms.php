<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bootsrtap Free Admin Template - SIMINTA | Admin Dashboad Template</title>
    <!-- Core CSS - Include with every page -->
    <link href="assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
   <link href="assets/css/style.css" rel="stylesheet" />
      <link href="assets/css/main-style.css" rel="stylesheet" />



</head>


<body>
    <?php
    include "konek.php";
    
    
?>
    <!--  wrapper -->
    <div id="wrapper">
        <!-- navbar top -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
            <!-- navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">
                    <img src="assets/img/logo2.png" alt="" />
                </a>
            </div>
            <!-- end navbar-header -->
            <!-- navbar-top-links -->
            <ul class="nav navbar-top-links navbar-right">
                <!-- main dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="top-label label label-danger">3</span><i class="fa fa-envelope fa-3x"></i>
                    </a>
                    <!-- dropdown-messages -->
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong><span class=" label label-danger">Andrew Smith</span></strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong><span class=" label label-info">Jonney Depp</span></strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong><span class=" label label-success">Jonney Depp</span></strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- end dropdown-messages -->
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="top-label label label-success">4</span>  <i class="fa fa-tasks fa-3x"></i>
                    </a>
                    <!-- dropdown tasks -->
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- end dropdown-tasks -->
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="top-label label label-warning">5</span>  <i class="fa fa-bell fa-3x"></i>
                    </a>
                    <!-- dropdown alerts-->
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i>New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i>3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i>Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i>New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i>Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- end dropdown-alerts -->
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-3x"></i>
                    </a>
                    <!-- dropdown user-->
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i>Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                        </li>
                    </ul>
                    <!-- end dropdown-user -->
                </li>
                <!-- end main dropdown -->
            </ul>
            <!-- end navbar-top-links -->

        </nav>
        <!-- end navbar top -->

        <!-- navbar side -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <!-- sidebar-collapse -->
            <div class="sidebar-collapse">
                <!-- side-menu -->
                <ul class="nav" id="side-menu">
                    <li>
                        <!-- user image section-->
                        <div class="user-section">
                            <div class="user-section-inner">
                                <img src="assets/img/user.jpg" alt="">
                            </div>
                            <div class="user-info">
                                <div>Jonny <strong>Deen</strong></div>
                                <div class="user-text-online">
                                    <span class="user-circle-online btn btn-success btn-circle "></span>&nbsp;Online
                                </div>
                            </div>
                        </div>
                        <!--end user image section-->
                    </li>
                    <li class="sidebar-search">
                        <!-- search section-->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!--end search section-->
                    </li>
                    <li class="">
                        <a href="index.html"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>Charts<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="flot.html">Flot Charts</a>
                            </li>
                            <li>
                                <a href="morris.html">Morris Charts</a>
                            </li>
                        </ul>
                        <!-- second-level-items -->
                    </li>
                     <li>
                        <a href="timeline.html"><i class="fa fa-flask fa-fw"></i>Timeline</a>
                    </li>
                    <li>
                        <a href="tables.html"><i class="fa fa-table fa-fw"></i>Tables</a>
                    </li>
                    <li class="selected">
                        <a href="forms.html"><i class="fa fa-edit fa-fw"></i>Form Pendataan</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i>UI Elements<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="panels-wells.html">Panels and Wells</a>
                            </li>
                            <li>
                                <a href="buttons.html">Buttons</a>
                            </li>
                            <li>
                                <a href="notifications.html">Notifications</a>
                            </li>
                            <li>
                                <a href="typography.html">Typography</a>
                            </li>
                            <li>
                                <a href="grid.html">Grid</a>
                            </li>
                        </ul>
                        <!-- second-level-items -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i>Multi-Level Dropdown<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Second Level Item</a>
                            </li>
                            <li>
                                <a href="#">Second Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">Third Level Item</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Item</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Item</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Item</a>
                                    </li>
                                </ul>
                                <!-- third-level-items -->
                            </li>
                        </ul>
                        <!-- second-level-items -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-files-o fa-fw"></i>Sample Pages<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Blank Page</a>
                            </li>
                            <li>
                                <a href="login.html">Login Page</a>
                            </li>
                        </ul>
                        <!-- second-level-items -->
                    </li>
                </ul>
                <!-- end side-menu -->
            </div>
            <!-- end sidebar-collapse -->
        </nav>
        <!-- end navbar side -->
        <!--  page-wrapper -->
          <div id="page-wrapper">
            <div class="row">
                 <!-- page header -->
                <div class="col-lg-12">
                    <h3 class="page-header">DAFTAR PERTANYAAN PENDATAAN PERUMAHAN DAN PEMUKIMAN</h3>
                </div>
                <!--end page header -->
            </div>
            <form method="post" name="fm_pendataan" role="form">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            A. INFORMASI UMUM
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">

                                        <div class="form-group">
                                            <table border=0 width="100%">
                                                
                                                <tr>
                                                    <td><label>Provinsi</label></td>
                                                    <td><select name="prov">
                                                        <?php
                                                        $query = mysqli_query("select * from tb_prov");
                                                            while($data=mysqli_fetch_array($conn,$query)){
                                                        ?>
                                                            <option value="<?=$data[id_prov]?>"><?=$data[
                                                            nm_prov]?></option>
                                                          
                                                        <?php }?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label>Kab/Kota</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Kecamatan</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Luas Desa</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Jumlah Kepala RT</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Jumlah Penduduk</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Luas Laki-Laki</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Jumlah Wanita</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                            
                                           
                                            </table>
                                             
                                        </div>
                                        
                                                                           
                                </div>
                                <div class="col-lg-6">
                                    
                                        <div class="form-group">
                                            <table border=0 width="100%">
                                                <tr>
                                                    <td><label>Kelurahan/desa</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>RT/RW/Dusun</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Tanggal Pendataan</label></td>
                                                    <td><input type="date" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Titik Ordinat</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Jumlah Kepala KK</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Jumlah KK MBR</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Jumlah KK Miskin</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                            
                                           
                                            </table>
                                             
                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!--end of rows-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            B1. KEPADATAN BANGUNAN HUNIAN
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                        <div class="form-group">
                                            <table border=0 width="100%">
                                                <tr>
                                                    <td><label>1.</label></td>
                                                    <td width="60%"><label>Berapa luas wilayah RT/RW/dusun*?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Ha</td>
                                                </tr>
                                                <tr>
                                                    <td><label>2.</label></td>
                                                    <td><label>Berapa luas wilayah permukiman?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Ha</td>
                                                </tr>
                                                <tr>
                                                    <td><label>3.</label></td>
                                                    <td><label>Berapa jumlah rumah yang ada di wilayah RT/RW/dusun</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Ha</td>
                                                </tr>
                                                <tr>
                                                    <td><label>4.</label></td>
                                                    <td><label>Berapa jumlah total bangunan di wilayah RT/RW/dusun*?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Unit</td>
                                                </tr>
                                                <tr valign="top">
                                                    <td><label>5.</label></td>
                                                    <td><label>Berapa persentase luas kawasan permukiman yang terletak di
                                                    <br>wilayah dengan kemiringan lebih dari 15%?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">%</td>
                                                </tr>                                                                    
                                            </table>
                                             
                                        </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!-- end of rows-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            B2. KETERATURAN BANGUNAN HUNIAN
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                        <div class="form-group">
                                            <table border=0 width="100%">
                                                <tr>                                                    
                                                    <td width="60%"><label>1. Berapa jumlah bangunan hunian yang menghadap jalan lingkungan I atau II</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Unit</td>
                                                </tr>
                                                <tr>
                                                    <td><label>2. Berapa jumlah bangunan hunian yang menghadap sungai</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Unit</td>
                                                </tr>
                                                <tr>
                                                    <td><label>3. Berapa jumlah bangunan hunian yang berada diatas sempadan jalan/sungai</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Unit</td>
                                                </tr>                                                                                                               
                                            </table>
                                             
                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!--end of rows-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            B3. KELAYAKAN BANGUNAN HUNIAN
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                        <div class="form-group">
                                            <table border=0 width="100%">
                                                <tr>                                                    
                                                    <td width="60%"><label>1. Jumlah bangunan hunian yang lebih dari satu lantai</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Unit</td>
                                                </tr>
                                                <tr>
                                                    <td><label>2. Jumlah bangunan hunian Permanen</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Unit</td>
                                                </tr>
                                                <tr>
                                                    <td><label>3. Jumlah bangunan hunian semi permanen</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Unit</td>
                                                </tr><tr>
                                                    <td><label>4. Jumlah bangunan tidak permanen</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Unit</td>
                                                </tr>  <tr>
                                                    <td><label>5. Jumlah bangunan dengan kondisi aladin baik</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Unit</td>
                                                </tr>                                                                                                               
                                            </table>
                                             
                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!--end of rows-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            B4. JALAN LINGKUNGAN
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form">
                                        <div class="form-group">
                                            <table border=0 width="100%">
                                                <tr>                                                    
                                                    <td width="60%"><label>1. Berapa panjang total jaringan jalan lingkungan?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">meter</td>
                                                </tr>
                                                <tr>
                                                    <td><label>2. Berapa luas area pemukiman yang belum terlayani jalan</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">meter</td>
                                                </tr>
                                                <tr>
                                                    <td><label>3. Berapa panjang jalan lingkungan yang permukaannya diperkeras?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">meter</td>
                                                </tr><tr>
                                                    <td><label>4. Berapa panjang jalan lingkungan  yang permukaannya diperkeras dan tidak rusak?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">meter</td>
                                                </tr>  <tr>
                                                    <td><label>5. Berapa panjang jalan lingkungan yang dilengkapi saluran samping jalan?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">meter</td>
                                                </tr>                                                                                                               
                                            </table>
                                             
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!--end of rows-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            B5. DRAINASE LINGKUNGAN
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form">
                                        <div class="form-group">
                                            <label>1. Berapa tinggi genangan rata-rata ?</label><br><br>
                                            <input type="radio" name=a value=a>Tidak pernah terjadi genangan<br>
                                            <input type="radio" name=a value=a>Tinggi genangan < 30 cm<br>
                                            <input type="radio" name=a value=a>Tinggi genangan >30 cm
                                            <br><br>
                                            <label>2. Berapa durasi genangan air/ banjir rata-rata?</label><br><br>
                                            <input type="radio" name=a value=a>Lama genangan < 2 jam<br>
                                            <input type="radio" name=a value=b>Lama genangan >2 jam <br>
                                            <br><br>
                                            <label>3. Berapa frekuensi genangan air/ banjir?</label><br><br>
                                            <input type="radio" name=a value=a>Terjadi < 2 kali/tahun<br>
                                            <input type="radio" name=a value=b>Terjadi  >2 kali/tahun  <br><br>
                                            <table width="100%">
                                                <tr>
                                                    <td width="60%"><label>4. Berapa luas area genangan air/ banjir dalam permukiman?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">Ha</td>
                                                </tr>
                                            </table>
                                            <br><br>
                                            <label>5. Apa sumber genangan air/ banjir?</label><br><br>
                                            <input type="radio" name=a value=a> Rob/Pasang air laut<br>
                                            <input type="radio" name=a value=b> Air sungai/danau/rawa <br>
                                            <input type="radio" name=a value=c> Limpasan air hujan/ air buangan rumah tangga
                                            <br><br>
                                            <table width="100%">
                                                <tr>
                                                    <td width="60%"><label>6. Berapa luas area yang tidak mempunyai drainase</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">meter</td>
                                                </tr>
                                                <tr>
                                                    <td width="60%"><label>7. Berapa panjang total drainase?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">meter</td>
                                                </tr>
                                                <tr>
                                                    <td width="60%"><label>8. Berapa panjang drainase dengan kondisi tidak rusak/berfungsi baik?</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="center">meter</td>
                                                </tr>
                                                <tr>
                                                    <td width="60%"><label>9. Apakah saluran drainase terhubung dengan sistim drainase yang ada</label></td>
                                                    <td><input class="form-control"></td>
                                                    <td align="left"></td>
                                                </tr>
                                            </table>
                                             
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!--end of rows-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            B6. SANITASI LINGKUNGAN
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form">
                                        <div class="form-group">
                                            <table border=0 width="100%">
                                                <tr>                                                    
                                                    <td width="60%"><label>1. Berapa Jumlah rumah tangga yang punya jamban sendiri</label></td>
                                                    <td><input class="form-control"></td>                                                    
                                                </tr>
                                                <tr>
                                                    <td><label>2. Berapa jumlah rumah tangga yang menggunakan MCK umum</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><label>3. Berapa jumlah rumah tangga yang tidak punya MCK</label></td>
                                                    <td><input class="form-control"></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td><label>4. Berapa jumlah rumah tangga yang menggunakan kloset leher angsa </label></td>
                                                    <td><input class="form-control"></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td><label>5. Berapa jumlah rumah tangga yang menggunakan kloset cemplungan </label></td>
                                                    <td><input class="form-control"></td>                                                
                                                </tr>
                                                <tr>
                                                    <td><label>6. Berapa jumlah rumah tangga yang punya septitanck </label></td>
                                                    <td><input class="form-control"></td>                                                
                                                </tr>
                                                <tr>
                                                    <td><label>7. Berapa jumlah rumah tangga yang tidak punya septitanck </label></td>
                                                    <td><input class="form-control"></td>                                                
                                                </tr>
                                                <tr>
                                                    <td><label>8. Apakah buangan limbah cair rumah tangga terpisah dengan saluran drainase? </label></td>
                                                    <td><input type="radio" name=a value=a> Ya   <input type="radio" name=a value=a> Tidak</td>                                                
                                                </tr> 
                                            </table>
                                             
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!--end of rows-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            B7. AIR MINUM
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form">
                                        <div class="form-group">
                                            <table border=0 width="100%">
                                                <tr>
                                                    <td valign="top"><label>1.</label></td>
                                                    <td width="68%"><label>Berapa jumlah rumah tangga yang menggunakan PDAM / perpipaan mata air /sumur bor berbayar <br>untuk air minum dan air bersih</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><label>2.</label></td>
                                                    <td><label>Berapa jumlah rumah tangga yang menggunakan  mata air tidak terlindungi/sungai/danau/ <br>air hujan untuk air minum dan air bersih</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><label>3.</label></td>
                                                    <td><label>Berapa jumlah rumah tangga yang menggunakan Air galon/ air tangki atau gerobak untuk air minum dan air bersih</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><label>4.</label></td>
                                                    <td><label>Berapa jumlah rumah tangga yang tercukupi air minum dan air bersih sepanjang tahun</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                            </table>
                                             
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!--end of rows-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            B8. PENGAMANAN BAHAYA KEBAKARAN
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form">
                                        <div class="form-group">
                                            <label>1. Berapa frekuensi kejadian kebakaran di lingkungan permukiman?</label><br><br>
                                            <input type="radio" name=a value=a>Tidak pernah terjadi kebakaran dalam 5 tahun<br>
                                            <input type="radio" name=a value=b>1-2 kali dalam 5 tahun <br>
                                            <input type="radio" name=a value=c>> 2 kali dalam 5 tahun 
                                            <br><br>
                                            <label>2. Apa penyebab kejadian bencana kebakaran?</label><br><br>
                                            <input type="radio" name=a value=a>Tungku/kompor masak<br>
                                            <input type="radio" name=a value=b>Konsleting listrik <br>
                                            <input type="radio" name=a value=c>Kebakaran hutan/ilalang <br>
                                            <input type="radio" name=a value=d>Pembakaran sampah <br>
                                            <input type="radio" name=a value=e> Lainnya<br>
                                            <br><br>
                                            <label>3. Apakah ada sarana pencegahan bahaya kebakaran?</label><br><br>
                                            <input type="radio" name=a value=a>Pos/Stasiun pemadam kebakaran<br>
                                            <input type="radio" name=a value=b>Hidran air/ sumber air lain yang terbuka<br>
                                            <input type="radio" name=a value=c>Mobil/ motor pemadam kebakaran/ APAR <br>
                                            <input type="radio" name=a value=d>Tidak ada <br>
                                            
                                            <br><br>
                                            <label>4. Apakah tersedia jalan dengan lebar minimal 3,5 meter di lingkungan permukiman dengan radius rumah terjauh kurang dari 100 m? </label><br><br>
                                            <input type="radio" name=a value=a> Ada<br>
                                            <input type="radio" name=a value=b> Tidak <br>
                                            
                                            <br><br>
                                            <label>5. Apakah tersedia pasokan air yang cukup saat terjadi kebakaran </label><br><br>
                                            <input type="radio" name=a value=a> Ada<br>
                                            <input type="radio" name=a value=b> Tidak <br>
                                            
                                             
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!--end of rows-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            B9. PERSAMPAHAN
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form">
                                        <div class="form-group">
                                            <table border=0 width="100%">
                                                <tr>
                                                    <td valign="top"><label>1.</label></td>
                                                    <td width="68%"><label>Berapa jumlah rumah tangga yang membuang sampah ke tempat sampah pribadi/TPS</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><label>2.</label></td>
                                                    <td><label>Berapa jumlah rumah tangga yang membuang sampah ke lubang /ruang terbuka dan dibakar</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><label>3.</label></td>
                                                    <td><label>Berapa RT yang mempunyai TPS</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><label>4.</label></td>
                                                    <td><label>Berapa RT yang mempunyai pengelolaan 3R</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><label>5.</label></td>
                                                    <td><label>Berapa RT yang melakukan pengangkutan sampah minimal 2 x dalam seminggu</label></td>
                                                    <td><input class="form-control"></td>
                                                </tr>
                                            </table>
                                             
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div><!--end of rows-->
            <p align="right">
            <input type="submit" class="btn btn-primary" value="Submit Button">
            <input type="reset" class="btn btn-success" value="Reset Button"></p>
            </form>
        </div>
          
        <!-- end page-wrapper -->
                    

    </div>
    <!-- end wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="assets/plugins/jquery-1.10.2.js"></script>
    <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="assets/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/plugins/pace/pace.js"></script>
    <script src="assets/scripts/siminta.js"></script>

</body>

</html>
