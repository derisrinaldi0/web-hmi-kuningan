
<?php
$id ="HMI".date('ymdhts');
?>
<div class="wrapper row3">
    <div class="rounded">
        <div class="container clear">
        <div class="group btmspace-50"  >
            <div class="one_quarter first">
            <h6>Data Registrasi</h6>
			note :<br>
			<ol type="1">
			<li>Harus beragama Islam</li>
			<li>Tidak cacat mental</li>
			<li>Tidak cacat kriminal</li>
			</ol>
            </div>
            <div class="one_half"> 
                <h1><b>Isi Form dibawah</b></h1>
              <div id="comments">
                  <form action="act_add_anggota.php" method="post">
                    <div class="group">
                        <label> Id Anggota :</label>
                        <input type="text" readonly value="<?=$id?>" name="id">
                    </div>
                    <div class="group">
                        <label> Nama :</label>
                        <input type="text" name="nama" required>
                    </div>
                    <div class="group">
                        <label> Tanggal Lahir :</label>
                        <input type="date" name="tgl_lahir" placeholder="<?=date('Y-m-d')?>" required>
                    </div>
                    <div class="group">
                        <label> Alamat  :</label>
                        <textarea name="alamat" required></textarea>
                    </div>
                    <div class="group">
                        <label> Perguruan Tinggi / Prodi :</label>
                        <input type="text" name="skl" required>
                    </div>
                    <div class="group">
                        <label> Tingkat :</label>
                        <input type="number" name="skl_tingkat" required>
                    </div>
                    <div class="group">
                        <label> Moto Hidup :</label>
                        <input type="text" name="moto" required>
                    </div>
                    <div class="group">
                        <label> Username :</label>
                        <input type="text" name="user" required>
                    </div>
                    <div class="group">
                        <label> Password :</label>
                        <input type="password" name="pass" required>
                    </div>
                    <div class="group">
                  
                        <input type="submit" name="save" value="Simpan">
                        <input type="reset"  value="Reset" style="float:right">
                    </div>
                </form>
                </div>
            </div>
            
        </div>
        </div>
    </div>x

</div>