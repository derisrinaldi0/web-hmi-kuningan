<!DOCTYPE html>
<!--
Template Name: Academic Education V2
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<html>
<head>
    
<title>HMI Kuningan</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top" style="background-color:green">
<!-- ################################################################################################ --> 
<!-- ################################################################################################ --> 
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="clear" style="background-color:green"> 
    <!-- ################################################################################################ -->
    <!--<nav>
      <ul>
        <li><a href="index.php?href=home">Home</a></li>
        <li><a href="#">Contact Us</a></li>
        <li><a href="#">A - Z Index</a></li>
        <li><a href="#">Student Login</a></li>
        <li><a href="#">Staff Login</a></li>
      </ul>
    </nav> -->
    <!-- ################################################################################################ --> 
  </div>
</div>
<!-- ################################################################################################ --> 
<!-- ################################################################################################ --> 
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="clear" style="background-color:green"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
  
      <h1><a href="index.php">Himpunan Mahasiswa Islam Kuningan</a></h1>
      <h3 class="hh"><a href="index.php">HMI Cabang Kuningan</a></h3>
      <p>YAKIN USAHA SAMPAI</p>
    </div>
    <div class="fl_right">
      <!--<form class="clear" method="post" action="#">
        <fieldset>
          <legend>Search:</legend>
          <input type="text" value="" placeholder="Search Here">
          <button class="fa fa-search" type="submit" title="Search"><em>Search</em></button>
        </fieldset>
      </form>-->
    </div>
    <!-- ################################################################################################ --> 
  </header>
</div>
<!-- ################################################################################################ --> 
<!-- ################################################################################################ --> 
<!-- ################################################################################################ -->
<div class="wrapper row2">
  <div class="rounded">
    <nav id="mainav" class="clear"> 
      <!-- ################################################################################################ -->
      <ul class="clear">
        <li class="<?=$active1?>"><a href="index.php">Home</a></li>
		<li class="<?=$active3?>"><a href="index.php?href=Berita">Berita</a></li>
		<li class="<?=$active4?> drop"><a href="#">Profil</a>
			<ul>
			<li><a href="index.php?href=visi">Visi Misi</a></li>
			<li><a href="index.php?href=tugas">Tugas Pokok dan Fungsi</a></li>
			<li><a href="index.php?href=poker">Program Kerja</a></li>
			<li><a href="index.php?href=str">Struktur Organisasi</a></li>
			<li><a href="index.php?href=pgw">Keanggotaan</a></li>
			</ul>
		</li>
		<!--<li class="<?=$active5?> drop"><a href="#">Potensi</a>
		<ul>
		<?php 
		$result=mysql_query("select * from potensi");
		while($data=mysql_fetch_array($result)){
		?>
			<li><a href="index.php?href=Potensi&id_potensi=<?=$data['id_potensi']?>"><?=$data['nama_potensi']?></a></li>
		<?php } ?>
			</ul>
		</li>-->
		<li class="<?=$active2?>"><a href="index.php?href=galeri">Gallery</a></li>
         <li class="<?=$active7?>"><a href="index.php?href=anggota">Data Diri</a></li>
        
        
        <?php
        $rslt="select * from menu where status='1'";
            $query=mysql_query($rslt);
            while($d=mysql_fetch_array($query)){
                ?>
            
        <li class="<?=$active6?>"><a href="index.php?href=<?=$d['nm']?>"><?=$d['nm']?></a></li>
        <?php }?>
        </ul>
      
      <!-- ################################################################################################ --> 
    </nav>
  </div>
</div>
<!-- ###################################################################### -->
 
    <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="assets/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/plugins/pace/pace.js"></script>
    <script src="assets/scripts/siminta.js"></script>
    <!-- Page-Level Plugin Scripts -->
    <script src="assets/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/plugins/morris/morris.js"></script>
    <script src="assets/scripts/dashboard-demo.js"></script>	
</body>
</html>