-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2018 at 06:21 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kec_kuningan`
--
CREATE DATABASE IF NOT EXISTS `kec_kuningan` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kec_kuningan`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `level` varchar(10) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_user`, `username`, `password`, `nama_user`, `level`) VALUES
(20, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Operator', 'operator'),
(21, 'bpmd', 'e98a4b6643d5f8b5fd1e3b69085861a0', 'BPMD', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

DROP TABLE IF EXISTS `anggota`;
CREATE TABLE IF NOT EXISTS `anggota` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(20) DEFAULT NULL,
  `tgl_lahir` text,
  `alamat` varchar(50) DEFAULT NULL,
  `skl` varchar(50) DEFAULT NULL,
  `skl_tingkat` int(11) DEFAULT NULL,
  `moto` varchar(50) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `pass` varchar(50) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id`, `nama`, `tgl_lahir`, `alamat`, `skl`, `skl_tingkat`, `moto`, `user`, `pass`, `status`) VALUES
('HMI180204102804', 'Dudi', '2018-02-07', 'cidahu', 'uniku', 2, 'sdfasdf', 'dudi', '2615726de7dfb59c869b398b415c6bda', 1),
('HMI180204102837', 'reza', '2018-02-15', 'asdfasdf', 'asdfsadf', 1, 'sdfasdf', 'eza', '3ed6e995474bc6dddef7a6fc9b97c965', 1);

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

DROP TABLE IF EXISTS `berita`;
CREATE TABLE IF NOT EXISTS `berita` (
  `id_berita` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `berita` text NOT NULL,
  `tanggal` text NOT NULL,
  `gambar` text NOT NULL,
  `penulis` varchar(20) NOT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `judul`, `berita`, `tanggal`, `gambar`, `penulis`) VALUES
(12, 'HMI Rohingia', '<p>HMI mengadakan acara untuk menyumbang dana ke rohingia</p>\r\n', '2016-12-02', 'HMI-Rohingya-.jpg', 'HMI'),
(13, 'HMI Rohingia', '<p>HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;HMI mengadakan acara himpunan dana untuk rohingia HMI mengadakan acara himpunan dana untuk rohingia&nbsp;</p>\r\n', '2017-12-31', 'HMI-Rohingya-.jpg', 'Operator'),
(14, 'sdfasd', '<p>sdfasdfasdfasdfasdfasdfasdf</p>\r\n', '2018-02-04', 'splash.png', 'dfgsdf');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

DROP TABLE IF EXISTS `galeri`;
CREATE TABLE IF NOT EXISTS `galeri` (
  `nama_gambar` text NOT NULL,
  `sumber` varchar(100) NOT NULL,
  `keterangan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`nama_gambar`, `sumber`, `keterangan`) VALUES
('konfercab-hmi-kuningan-deadlock-persidangan-mandeg-dua-hari-jpg.jpg', 'HMI', 'HMI'),
('IMG-20171231-WA0007.jpg', 'hmi', 'calon ketua');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

DROP TABLE IF EXISTS `kelurahan`;
CREATE TABLE IF NOT EXISTS `kelurahan` (
  `id_kelurahan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelurahan` varchar(15) NOT NULL,
  `laki_laki` int(11) NOT NULL,
  `perempuan` int(11) NOT NULL,
  `wilayah` int(11) NOT NULL,
  PRIMARY KEY (`id_kelurahan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id_kelurahan`, `nama_kelurahan`, `laki_laki`, `perempuan`, `wilayah`) VALUES
(1, 'Kuningan', 5877, 6030, 301),
(3, 'Purwawinangun', 6940, 6983, 236),
(4, 'Cirendang', 2527, 2450, 207);

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

DROP TABLE IF EXISTS `komentar`;
CREATE TABLE IF NOT EXISTS `komentar` (
  `id_komentar` int(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `e-mail` varchar(50) NOT NULL,
  `komentar` text NOT NULL,
  `tanggal` text NOT NULL,
  `id_berita` int(5) NOT NULL,
  PRIMARY KEY (`id_komentar`),
  KEY `id_berita` (`id_berita`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `nama`, `e-mail`, `komentar`, `tanggal`, `id_berita`) VALUES
(1480435312, 'dsf', 'derisrinaldi@ymail.com', 'asdfasdfasdfa', 'Tue,29-Nov-2016', 8),
(1480435429, 'dsf', 'aasdasd', 'asdada', 'Tue,29-Nov-2016', 8),
(1480435466, 'sdfsdf', 'sdfsdfs', 'sdfsdfs', 'Tue,29-Nov-2016', 6),
(1480704377, 'deri', 'derisrinaldi@ymail.com', 'hahah bagus', 'Fri,02-Dec-2016', 9),
(1480915824, 'fahmi', 'fahmi.triple.ef@gmail.com', 'wanitanya cantik cantik', 'Mon,05-Dec-2016', 9);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `nm`, `status`) VALUES
(1, 'Registrasi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `potensi`
--

DROP TABLE IF EXISTS `potensi`;
CREATE TABLE IF NOT EXISTS `potensi` (
  `id_potensi` int(5) NOT NULL,
  `nama_potensi` varchar(50) NOT NULL,
  `potensi` text NOT NULL,
  PRIMARY KEY (`id_potensi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `potensi`
--

INSERT INTO `potensi` (`id_potensi`, `nama_potensi`, `potensi`) VALUES
(1, 'Tingkat Pendidikan', '<ol>\r\n	<li style="list-style-type:none"><strong>Tingkat Pendidikan</strong></li>\r\n</ol>\r\n\r\n<p style="margin-left:42.55pt; text-align:justify">Tidak Sekolah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : -</p>\r\n\r\n<p style="margin-left:42.55pt; text-align:justify">Tamat SD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 1.690 Siswa</p>\r\n\r\n<p style="margin-left:42.55pt; text-align:justify">Tamat SMP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 2.036 Siswa</p>\r\n\r\n<p style="margin-left:42.55pt; text-align:justify">Tamat SLTA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 2.492 Siswa</p>\r\n\r\n<p style="margin-left:42.55pt; text-align:justify">Tamat PT/Akademi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</p>\r\n'),
(2, 'Sarana dan Prasarana', '<p style="list-style-type:none"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Sarana dan Prasarana</span></strong></p>\r\n\r\n<ul>\r\n	<li style="list-style-type: none;"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Pendidikan</span></strong></li>\r\n</ul>\r\n\r\n<p style="margin-left:60.65pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah SD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 53 buah</span></p>\r\n\r\n<p style="margin-left:60.65pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah MI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 3 Buah</span></p>\r\n\r\n<p style="margin-left:60.65pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah SMP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 7 Buah</span></p>\r\n\r\n<p style="margin-left:60.65pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah MTS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 2 Buah</span></p>\r\n\r\n<p style="margin-left:60.65pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah SMA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 4 Buah</span></p>\r\n\r\n<p style="margin-left:60.65pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah MA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : - Buah</span></p>\r\n\r\n<p style="margin-left:60.65pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah SMK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 8 Buah</span></p>\r\n\r\n<p style="margin-left:60.65pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah LPK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : - Buah</span></p>\r\n\r\n<p style="margin-left:60.65pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah PT/Akademi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 2 Buah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>\r\n\r\n<ul>\r\n	<li style="list-style-type: none;">â€‹â€‹â€‹â€‹â€‹â€‹â€‹<strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Agama</span></strong></li>\r\n</ul>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah Mesjid Jami&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 16 Buah</span></p>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah Pontren&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp; 4 Buah</span></p>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah Gereja&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp; 4 Buah</span></p>\r\n\r\n<ul>\r\n	<li style="list-style-type: none;">â€‹â€‹â€‹â€‹â€‹â€‹â€‹<strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Kesehatan</span></strong></li>\r\n</ul>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah Posyandu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 104 Buah</span></p>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah Poskesdes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp; 6 Buah</span></p>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah Puskesmas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp; 3 Buah</span></p>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah Puskes Pembantu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp; 5 Buah</span></p>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jumlah balai Pengobatan Swasta&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp; 13 Buah</span></p>\r\n\r\n<ul>\r\n	<li style="list-style-type: none;">â€‹â€‹â€‹â€‹â€‹â€‹â€‹â€‹â€‹â€‹â€‹â€‹â€‹â€‹<strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Infrastruktur</span></strong></li>\r\n</ul>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jalan Propinsi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 7 KM</span></p>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jalan Kabupaten&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 58,37&nbsp;&nbsp; KM</span></p>\r\n\r\n<p style="margin-left:63.8pt; text-align:justify"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jalan Desa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 26,152 KM </span></p>\r\n'),
(3, 'Wisata dan Budaya', '<ol>\r\n	<li style="list-style-type:none"><strong>Wisata dan Budaya</strong></li>\r\n</ol>\r\n\r\n<ul>\r\n	<li style="list-style-type:none"><strong>Objek Daerah Tujuan Wisata</strong></li>\r\n</ul>\r\n\r\n<table border="1" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:solid black 1.0pt; margin-left:5.4pt">\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:1.0cm">\r\n			<p style="margin-left:0cm; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">No</span></strong></p>\r\n			</td>\r\n			<td style="width:219.75pt">\r\n			<p style="margin-left:0cm; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Nama Objek Wisata</span></strong></p>\r\n			</td>\r\n			<td style="width:170.9pt">\r\n			<p style="margin-left:0cm; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Lokasi</span></strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:1.0cm">\r\n			<p style="margin-left:0cm; text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td style="width:219.75pt">\r\n			<p style="margin-left:-90.45pt; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">H&nbsp; I&nbsp; H&nbsp; I&nbsp; L</span></strong></p>\r\n			</td>\r\n			<td style="width:170.9pt">\r\n			<p style="margin-left:0cm; text-align:center">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style="text-align:justify"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Kesenian Tradisional</span></strong></p>\r\n\r\n<table border="1" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:solid black 1.0pt; margin-left:5.4pt">\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:42.5pt">\r\n			<p style="margin-left:0cm; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">No</span></strong></p>\r\n			</td>\r\n			<td style="width:219.75pt">\r\n			<p style="margin-left:0cm; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Nama Kesenian/Budaya</span></strong></p>\r\n			</td>\r\n			<td style="width:170.9pt">\r\n			<p style="margin-left:0cm; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Lokasi</span></strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:42.5pt">\r\n			<p style="margin-left:0cm; text-align:center"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">1</span></p>\r\n\r\n			<p style="margin-left:0cm; text-align:center"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2</span></p>\r\n\r\n			<p style="margin-left:0cm; text-align:center"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">3</span></p>\r\n\r\n			<p style="margin-left:0cm; text-align:center"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">4</span></p>\r\n\r\n			<p style="margin-left:0cm; text-align:center"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">5</span></p>\r\n			</td>\r\n			<td style="width:219.75pt">\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Genjring</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Silat</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Rudat</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Gembyung</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Calung</span></p>\r\n			</td>\r\n			<td style="width:170.9pt">\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Winduhaji</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Ancaran</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Ciporang</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Ancaran</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Citangtu</span></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<ol>\r\n	<li style="list-style-type:none"><strong>Ekonomi dan Industri</strong></li>\r\n</ol>\r\n\r\n<table border="1" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:solid black 1.0pt; margin-left:5.4pt">\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:1.0cm">\r\n			<p style="margin-left:0cm; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">No</span></strong></p>\r\n			</td>\r\n			<td style="width:233.9pt">\r\n			<p style="margin-left:0cm; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jenis Industri</span></strong></p>\r\n			</td>\r\n			<td style="width:170.9pt">\r\n			<p style="margin-left:0cm; text-align:center"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Lokasi</span></strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:1.0cm">\r\n			<p style="margin-left:0cm"><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">1</span></strong></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">3</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">4</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">5</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">6</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">7</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">8</span></p>\r\n\r\n			<p style="margin-left:0cm">&nbsp;</p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">9</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">10</span></p>\r\n			</td>\r\n			<td style="width:233.9pt">\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Bawang Goreng</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Emping</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Kupat/Leupeut/Koecang</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Gemblong/Anyaman</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Opak Bakar</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jamur/Kerajinan Perak</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Tahu</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Minuman Ringan dari jahe/makanan Olah Ubi</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Jeli</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Anyaman</span></p>\r\n			</td>\r\n			<td style="width:170.9pt">\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Cirendang</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Karangtawang</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Winduhaji</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Citangtu</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Cijoho</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Ancaran</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Kuningan</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Kedungarum</span></p>\r\n\r\n			<p style="margin-left:0cm">&nbsp;</p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Ciporang</span></p>\r\n\r\n			<p style="margin-left:0cm"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Cibinuang</span></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

DROP TABLE IF EXISTS `profil`;
CREATE TABLE IF NOT EXISTS `profil` (
  `id_profil` int(5) NOT NULL,
  `nama_profil` varchar(50) NOT NULL,
  `isi_profil` text NOT NULL,
  PRIMARY KEY (`id_profil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id_profil`, `nama_profil`, `isi_profil`) VALUES
(1, 'Visi dan Misi', '<p>dsfasdfasdfa</p>\r\n'),
(2, 'Tugas Pokok dan Fungsi', '<p><strong>ddd</strong></p>\r\n'),
(3, 'Struktur Organisasi', 'logouniku.jpg'),
(4, 'Program Kerja', '<p>reza</p>\r\n'),
(5, 'Keanggotaan', '<table border="1" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:solid black 1.0pt; margin-left:5.4pt; width:693px">\r\n	<tbody>\r\n		<tr>\r\n			<td rowspan="3" style="width:63.8pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">BIDTUGAS</span></span></p>\r\n			</td>\r\n			<td colspan="3" style="width:45.05pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">KATAGORI ESELON</span></span></p>\r\n			</td>\r\n			<td colspan="3" style="width:99.2pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">JUMLAH PELAKSANA</span></span></p>\r\n			</td>\r\n			<td colspan="12" style="width:302.25pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">JUMLAH PEGAWAI BERDASARKAN</span></span></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan="2" style="width:21.25pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">II</span></span></p>\r\n			</td>\r\n			<td rowspan="2" style="width:21.3pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">III</span></span></p>\r\n			</td>\r\n			<td colspan="2" rowspan="2" style="width:11.8pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">IV</span></span></p>\r\n			</td>\r\n			<td rowspan="2" style="width:35.4pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">PNS</span></span></p>\r\n			</td>\r\n			<td colspan="2" rowspan="2" style="width:63.8pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">HONORER</span></span></p>\r\n			</td>\r\n			<td colspan="4" style="width:92.15pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">GOLONGAN</span></span></p>\r\n			</td>\r\n			<td colspan="8" style="width:210.1pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">PENDIDIKAN</span></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:21.25pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">I</span></span></p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">II</span></span></p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">III</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">IV</span></span></p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">SD</span></span></p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">SLTP</span></span></p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">SLTA</span></span></p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">D-3</span></span></p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">D-4</span></span></p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">S-1</span></span></p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">S-2</span></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Kepala</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<ul>\r\n				<li style="text-align:center">&nbsp;</li>\r\n			</ul>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">27</span></span></p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">6</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">9</span></span></p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">16</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2</span></span></p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">1</span></span></p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p style="text-align:center"><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">10</span></span></p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">15</span></span></p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2</span></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Sekretariat</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<ul>\r\n				<li style="text-align:center">&nbsp;</li>\r\n			</ul>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Seksi Pem</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<ul>\r\n				<li style="text-align:center">&nbsp;</li>\r\n			</ul>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Seksi Ekbang</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<ul>\r\n				<li style="text-align:center">&nbsp;</li>\r\n			</ul>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Seksi PM</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<ul>\r\n				<li style="text-align:center">&nbsp;</li>\r\n			</ul>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Seksi Kesra</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<ul>\r\n				<li style="text-align:center">&nbsp;</li>\r\n			</ul>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Seksi Trantib</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<ul>\r\n				<li style="text-align:center">&nbsp;</li>\r\n			</ul>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Kasubag Umum</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<ul>\r\n				<li style="text-align:center">&nbsp;</li>\r\n			</ul>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Kasubag Keuangan</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<ul>\r\n				<li style="text-align:center">&nbsp;</li>\r\n			</ul>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Fungsional</span></span></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:63.8pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">JUMLAH</span></span></strong></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p style="text-align:center">&nbsp;</p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2</span></span></strong></p>\r\n			</td>\r\n			<td colspan="2" style="width:11.8pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">7</span></span></strong></p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">27</span></span></strong></p>\r\n			</td>\r\n			<td colspan="2" style="width:63.8pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">6</span></span></strong></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">-</span></span></strong></p>\r\n			</td>\r\n			<td style="width:21.3pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">9</span></span></strong></p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">16</span></span></strong></p>\r\n			</td>\r\n			<td style="width:21.25pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2</span></span></strong></p>\r\n			</td>\r\n			<td style="width:25.85pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">-</span></span></strong></p>\r\n			</td>\r\n			<td style="width:35.45pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">1</span></span></strong></p>\r\n			</td>\r\n			<td style="width:35.4pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">10</span></span></strong></p>\r\n			</td>\r\n			<td style="width:26.15pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">-</span></span></strong></p>\r\n			</td>\r\n			<td style="width:1.0cm">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">-</span></span></strong></p>\r\n			</td>\r\n			<td style="width:30.55pt">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">15</span></span></strong></p>\r\n			</td>\r\n			<td colspan="2" style="width:1.0cm">\r\n			<p style="text-align:center"><strong><span style="font-size:9.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2</span></span></strong></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `saran`
--

DROP TABLE IF EXISTS `saran`;
CREATE TABLE IF NOT EXISTS `saran` (
  `id_saran` int(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `e-mail` varchar(50) NOT NULL,
  `isi_saran` text NOT NULL,
  `tanggal` text NOT NULL,
  PRIMARY KEY (`id_saran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saran`
--

INSERT INTO `saran` (`id_saran`, `nama`, `e-mail`, `isi_saran`, `tanggal`) VALUES
(1480710276, 'Dderis', 'Deris@gmail.com', 'Webnya keren , kembangkan lagi untuk kedepannya :D', 'Fri,02-Dec-2016');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
